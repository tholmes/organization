This is a repo for scripts meant to help me organize my life and responsibilities.

To add a cron job to lxplus, use the command 
`acrontab -e`

This will open up a list of your scheduled cron jobs. The formatting is as follows:

`minute / hour / day / month / weekday [1 = Monday, 7 = Sunday] / host / command`

For example, it might look like this:
```
0 17 * * * lxplus.cern.ch python /afs/cern.ch/user/t/tholmes/useful_files/cron_scripts/integration_email.py 
0 6 * * 1 lxplus.cern.ch python /afs/cern.ch/user/t/tholmes/useful_files/cron_scripts/room_booking_email.py 
0 9 * * * lxplus.cern.ch bash /afs/cern.ch/user/t/tholmes/useful_files/cron_scripts/disk_usage.sh
0 9 * * 3 lxplus.cern.ch bash /afs/cern.ch/user/t/tholmes/useful_files/cron_scripts/planner_email.sh
0 9 * * 5 lxplus.cern.ch bash /afs/cern.ch/user/t/tholmes/useful_files/cron_scripts/planner_email.sh
```

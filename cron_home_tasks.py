
import reader
import email

colors = ['#581845', '#900c3f', '#c70039', '#ff5733', '#ffc300']

# Get Task list and parse
home_tasks_url = "https://docs.google.com/spreadsheets/d/e/2PACX-1vS0KKC9Ap75kNAiU7QNTEhxbn4DDs-RVHn5QajN-BXBjFDYwAU45N1vhIzjfoAoTNRL2M1Tg_HseSzj/pub?output=csv"
csv = reader.getGoogleSheet(home_tasks_url)
tlist = reader.parseSheet(csv)
print csv

# Add colors
for i, task in enumerate(tlist.TaskList):
    task.addColor(colors[i])

print tlist.printHTML()

# Send email
email = email.Email("tholmes@cern.ch, lawrenceleejr@gmail.com", "Hi!!", tlist.printHTML())
email.send()


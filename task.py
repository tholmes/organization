#!/usr/bin/env python

import subprocess
import datetime

# Make a task object
class Task:

    def __init__(self, task_id=None, task_desc=None, task_date=None, task_resp=None, task_done=None):
        self.Id = task_id
        self.Desc = task_desc
        self.Date = task_date
        self.Resp = task_resp
        self.Done = task_done
        self.Color = None
        self.SubTaskList = []

    def addTask(self, task):
        self.SubTaskList.append(task)

    def addColor(self, color):
        self.Color = color
        for task in self.SubTaskList:
            task.addColor(color)

    def hasSubTasks(self):
        if len(self.SubTaskList) > 0: return true
        return false

    def printHTML(self, n_sub=0):

        out_str = "<tr>"
        if self.Color != None:
            out_str = "<tr style=\"color:%s\">"%self.Color
            if n_sub == 0: out_str = "<tr style=\"color:white; background-color:%s\">"%self.Color

        out_str += "<th>%s</th>"%str(self.Date)
        if self.Resp == "":
            out_str += "<th>Assignee: %s</th>"%"--"
        else:
            out_str += "<th>Assignee: %s</th>"%self.Resp
        out_str += "<th>%s</th>"%self.Id
        out_str += "<th>%s</th>"%self.Desc
        out_str += "</tr>\n"
        for stask in self.SubTaskList:
            out_str +=  "\t" + stask.printHTML(n_sub+1)
        return out_str

class List:

    def __init__(self, name=None):
        self.Name = name
        self.TaskList = []

    def printHTML(self):
        out_str = ""
        for task in self.TaskList:
            out_str += task.printHTML()
            out_str += "\n"
        return out_str

    def addTask(self, task):
        if self.findTask(task.Id)==None:
            self.TaskList.append(task)
        else:
            print "WARNING: Failed to add task with duplicate ID", task.Id

    def findTask(self, task_id):
        for task in self.TaskList:
            if task.Id == task_id:
                return task
        return None




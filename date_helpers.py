#!/usr/bin/env python

import subprocess
import datetime

# Fiddle with dates
today = datetime.date.today()

def getDate(date_str):  # Reads in American dates
    d = date_str.split("/")
    if len(d) != 3: return None
    date = datetime.date(int(d[2]), int(d[0]), int(d[1]))    # Year, Month, Day
    return date

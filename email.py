#!/usr/bin/env python

import subprocess
import datetime
import os

# Add formatting things here
color_urgent = "#ff9966"
color_soon = "#ffcc00"
color_past = "#9966ff"

class Email():

    def __init__(self, recipient = None, subject = None, body = None, send_mail = True):
        self.recipient = recipient
        self.subject = subject
        self.body = body
        self.send_mail = send_mail

    # Send an email!
    def send(self, body_file="email.tmp"):

        with open(body_file, "w") as f:
            f.write(self.body)
        command_line = 'mutt -e \'set content_type="text/html"\' %s -s "%s" < %s'%(self.recipient, self.subject, body_file)

        if self.send_mail: process = subprocess.Popen(command_line, shell=True)
        else: subprocess.Popen('cat %s'%body_file)

        os.remove(body_file)


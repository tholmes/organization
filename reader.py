#!/usr/bin/env python

import subprocess
import datetime

# My classes
import task

def getGoogleSheet(url):
    csv = subprocess.check_output(["curl", url])
    return csv

def parseSheet(csv, name=None):

    # Get header order (TODO: is there a cleaner way to do this?)
    # Currently rely on having header names that match Task attributes
    headers = csv.split("\n")[0].split(",")
    n_headers = len(headers)-1      # Drop that last annoying ","

    # Make a new to do list
    tlist = task.List(name)
    for line in csv.split("\n"):

        # Skip the headers
        if line.startswith("Id"): continue
        # Skip empty lines
        if line.startswith(","): continue

        # Make a new task
        t = task.Task()
        cols = line.split(",")
        for x in xrange(n_headers):
            exec("t.%s = \"%s\""%(headers[x], cols[x]))

        if "." not in t.Id:
            tlist.addTask(t)
        else:
            parent_task = tlist.findTask(t.Id.split(".")[0])
            if parent_task == None: print "WARNING: Failed to find parent task for task id", t.Id
            else: parent_task.addTask(t)

    return tlist
